@Login
Feature: User attempts login with credentials

  Background:
    Given I am on home page


  Scenario: User successfully logs in with valid credentials
    When I enter "go2automationclass@gmail.com" and "Pass2016" details
    And I click the login button
    Then I see "Personal apps"

  Scenario Outline: User logs in with invalid credentials
    When I enter "<username>" and "<password>" details
    And I click the login button
    Then I see "<loginMessage>" error message

  Examples:
    |username                |password|loginMessage|
    |go2automationclass@gmail|Pass2016|There was a problem with your login.|
    |go2automationclass@gmail.com|Pas2016|There was a problem with your login.|
