package Pages;


import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by famos on 20/03/2017.
 */
public class LandingPage {

    protected WebDriver driver;


    @FindBy(xpath = "//*[text()='Personal apps']")public WebElement personalAppsAMssg;
    @FindBy(xpath = "//*[text()='There was a problem with your login.']")public WebElement errorAMssg;

    public LandingPage(WebDriver driver){
        this.driver=driver;
        PageFactory.initElements(driver, this);
    }

    public void checkMssg(String text){
        Assert.assertTrue(personalAppsAMssg.getText().equals(text));
    }

    public void checkErrorMssg(String error){
        Assert.assertTrue(errorAMssg.getText().equals(error));
    }

}
