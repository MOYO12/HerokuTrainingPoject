package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import steDefn.Hooks;

/**
 * Created by famos on 19/03/2017.
 */
public class LoginPage {

    private WebDriver driver;


    @FindBy(id = "email")public WebElement username;
    @FindBy(id = "password")public WebElement password;
    @FindBy(name = "commit")public WebElement loginBtn;


    public LoginPage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }


    public void loginDetails(String usname, String pwd){
        username.sendKeys(usname);
        password.sendKeys(pwd);
    }

    public void clickLogBtn(){
        loginBtn.click();
    }
}
