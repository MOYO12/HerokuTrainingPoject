package com.Base;

import Pages.LandingPage;
import Pages.LoginPage;
import org.openqa.selenium.WebDriver;
import steDefn.Hooks;

/**
 * Created by famos on 20/03/2017.
 */
public class BaseClass {

    public LoginPage loginPage;
    public LandingPage landingPage;
    protected WebDriver driver;

    public BaseClass(){
        this.driver = Hooks.driver;
        loginPage = new LoginPage(driver);
        landingPage = new LandingPage(driver);

    }
}
