package steDefn;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * Created by famos on 04/03/2017.
 */

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/resources",
        plugin = {"pretty", "json:target/cucumber.json"},
        tags = "@Login",
        monochrome = false
)
public class RunnerTest {
}