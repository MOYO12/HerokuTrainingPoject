package steDefn;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

/**
 * Created by famos on 19/03/2017.
 */
public class Hooks {

    public static WebDriver driver = null;


    @Before
    public WebDriver setup(){
        System.setProperty("webdriver.chrome.driver","C:\\Users\\User\\Documents\\ZLATAN\\ChromeDriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
        return driver;
    }

    @After
    public void teardown(){
        driver.quit();
    }
}
