package steDefn;

import Pages.LoginPage;
import com.Base.BaseClass;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import junit.framework.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 * Created by famos on 09/03/2017.
 */
public class LoginStep extends BaseClass{

    @Given("^I am on home page$")
    public void i_am_on_home_page() throws Throwable {
        System.out.println("I am on home page");
        driver.get("https://id.heroku.com/login");
    }

    @When("^I enter \"([^\"]*)\" and \"([^\"]*)\" details$")
    public void i_enter_and(String usname, String pwd) throws Throwable {
        System.out.println(usname + pwd);
        loginPage.loginDetails(usname, pwd);
    }

    @When("^I click the login button$")
    public void i_click_the_login_button() throws Throwable {
        System.out.println("Login Btn");
        loginPage.clickLogBtn();
    }

    @Then("^I see \"([^\"]*)\"$")
    public void i_see(String text) throws Throwable {

        System.out.println("Just Joined the Team");
        landingPage.checkMssg(text);

        //WebElement elemt = driver.findElement(By.xpath("//*[text()='Personal apps']"));
        //wait.until(ExpectedConditions.visibilityOf(elemt));
        //assert  elemt.getText().equals(text);
        //driver.quit();
    }

    @Then("^I see \"([^\"]*)\" error message$")
    public void i_see_error_message(String text) throws Throwable {
        System.out.println("Message = "+text);
        Thread.sleep(1000L);
        System.out.println("I see error message");
        landingPage.checkErrorMssg(text);
    }





}
